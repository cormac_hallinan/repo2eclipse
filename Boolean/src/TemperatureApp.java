import java.util.Scanner;

public class TemperatureApp {
	
	//create scanner object instance
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		boolean flag = true;
		
		while(flag == true){
			
			System.out.print("Enter Temperature : ");
			int temperature = sc.nextInt();
			
			if(temperature == 999){
				flag = false;
			//How hot is it??
				//temperatue greater than 60 and less than 70
			}else if (temperature >= 60 && temperature < 70){
				System.out.println("Very Hot");
				//temperatue greater than 50 and less than 60
			}else if (temperature >= 50 && temperature <= 59){
				System.out.println("Hot");
				//temperatue greater than 40 and less than 45 OR temperature greater than 46 and less 50
			}else if ((temperature >= 40 && temperature <= 45)||(temperature >=46 && temperature < 50)){
				System.out.println("Not Hot");
				//temperatue less than 40
			}else if (temperature < 40){
				System.out.println("less Hot");
			}else{
				System.out.println("invalid output");
			}	//END Else/If
			
			System.out.println();
			
		}	//END While Flag
		
		System.out.println("Program Ended");
		
	}	//END Void Main

}	//END Class TemperatureApp
